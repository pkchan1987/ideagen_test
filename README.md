# qhse-sac-interview
## What you need
* docker and docker-compose
* VS Code
* Remote - Containers extension

## Getting started
* Clone this repository to your local machine. e.g: c:\git\qhse-sac-interview
* If you have the folder opened in your VS Code, close it before proceed to next step.
* Build base images: 
  * Start Powershell with admin privilege.
  * Navigate to qhse-sac-interview folder: cd C:\bit\qhse-sac-interview
  * Execute script to initialise: ./win_dev_setup.ps1 repo_init
  * Execute script to build base images: ./win_dev_setup.ps1 docker_build_base_images
* Stop your local IIS or Apache and MySql instance before proceed to the next step (to free up port 80, 443 and 3306).
* Launch VSCode and open c:\git\qhse-sac-interview, wait few seconds (maybe minutes if your machine is slow) and you will be asked to reopen the folder in container.
* From VSCode, reopen the folder in container and give it a good 5-10 minutes (depending on your connection speed, it may take longer than this) for the containers to start and setup.
* At this point, you should be able to see 3 containers:
  * Yii
  * devcontainer
  * mysql
* Browse the application by http://127.0.0.99/
