# Helper script to get setup for 
Param (
    [string]$Task
)

function init_repo() {
    git submodule update --init
    pushd .docker\base\docker-phusion-baseimage;
    git config core.autocrlf input;
    git rm --cached -r .;
    git reset --hard HEAD;
    popd;
}

function build_base_images() {
    pushd .docker\base;
    docker-compose -f build.docker-compose.yml up --build;
    popd;
}

$tasks = @{
    "repo_init" = @{
        description = "`tPerforms actions to initialize the repository.`n`n" +
                      "`tPrimarily initializing git submodules and making sure`n" +
                      "`tline ending requirements are setup correctly.";
        func = "init_repo"
    };
    "docker_build_base_images" = @{
        description = "`tBuilds the base docker images that are required.`n`n" +
                      "`tdocker must be installed and docker-compose must be in PATH.";
        func = "build_base_images"
    }
}

if ($tasks.Keys -contains $Task) {
    &$tasks[$Task].func
}

if ($Task -eq "") {
    Write-Output "Usage: win_dev_setup -Task [task_name]`n";
    Write-Output "Available Tasks:";
    $tasks.Keys | %{
        Write-Output "$_";
        Write-Output $tasks[$_]['description'];
        Write-Output '';
    }
}