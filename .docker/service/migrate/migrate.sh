#!/bin/bash
echo "Installing App dependencies (required to perform migration)"
pushd /app/interview
export HOME=/root 
composer install
popd
echo "App dependencies installed"

while ! mysqladmin ping -h"$componentsMySqlHost" --silent; do
    echo "Yii Migration: Waiting for MySQL to come UP..."
    sleep 5
done
echo "Yii Migration: MySQL UP... Giving it a moment to get it together..."
sleep 5
php /app/interview/client/protected/yiic.php migrate up --interactive=0
