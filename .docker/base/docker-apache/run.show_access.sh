#!/bin/bash
if [ "$DEBUG_HTTP_REQUESTS" = "no" ];
then
    sleep infinity;
else
    tail -f /var/log/apache2/access.log
fi
