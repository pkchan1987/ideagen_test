FROM interview/apache:latest

# Unfortunately, Yii doesn't work like the rest of our projects and expects to be in /app/interview not /app
RUN rm -Rf /app;
ADD ./ /app/interview

# Change some expectations in Apache & PHP to suit Yii, such as:
# * Apache site directory
# * PHP Short tags
# * error_reporting levels
# * Fix permissions
# * Make sure phusion can start all the run scripts in /etc/service/*/run
RUN sed -i "s/\/var\/www\/html\/public/\/var\/www\/html/g" /etc/apache2/sites-enabled/000-default.conf \
 && rm -R /var/www/html \
 && ln -s /app/interview/client /var/www/html \
 && sed -i "s/short_open_tag\ =\ Off/short_open_tag\ =\ On/g" /etc/php/7.2/apache2/php.ini \
 && sed -i "s/error_reporting.*/error_reporting = E_ALL \& \~E_DEPRECATED \& \~E_STRICT \& \~E_CORE_WARNING/g" /etc/php/7.2/apache2/php.ini \
 && chmod 777 -R /app/interview/client/protected/runtime/ \
 && chmod 777 -R /app/interview/client/assets \
 && find /app/interview/client/uploads -type d -exec chmod 777 {} +

#RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
#RUN apt-get install -y nodejs
#
#RUN cd /app/interview \
#    && npm install \
#    && npm run build:prod

# Add Services
ADD .docker/service /etc/service
COPY .docker/healthcheck /usr/local/bin/

RUN chmod +x /etc/service/*/run \
    && chmod +x /etc/service/*/*.sh \
    && chmod +x /usr/local/bin/healthcheck \
    && sed -i 's/\r$//' /etc/service/*/run \
    && sed -i 's/\r$//' /etc/service/*/*.sh \
    && sed -i 's/\r$//' /usr/local/bin/healthcheck
