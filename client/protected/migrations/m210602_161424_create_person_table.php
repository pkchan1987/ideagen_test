<?php

class m210602_161424_create_person_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_person', array(
            'id' => 'pk AUTO_INCREMENT',
			'username' => 'varchar(128)',
			'password' => 'varchar(128)'
        ));
	}

	public function down()
	{
		echo "m210602_161424_create_person_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}