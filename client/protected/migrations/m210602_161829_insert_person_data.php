<?php

class m210602_161829_insert_person_data extends CDbMigration
{
	public function safeUp()
	{
        $hash_key = 'nEnRK68SLNOieDTwqTEW83RZL9PjmTfT';

        $this->insert('tbl_person', [
            'username' => 'demo',
            'password' => md5('demo'. $hash_key),
        ]);

        $this->insert('tbl_person', [
            'username' => 'admin',
            'password' => md5('admin'.$hash_key)
        ]);

	}

	public function safeDown()
	{
        echo "m210602_161829_insert_person_data does not support migration down.\n";
		return false;
	}
}