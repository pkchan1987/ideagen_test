<?php

class Person extends CActiveRecord
{

    public function tableName()
	{
		return 'tbl_person';
	}

    public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password'
		);
	}

    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}