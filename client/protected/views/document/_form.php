<?php
/* @var $this DocumentController */
/* @var $model Document */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'document-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Number'); ?>
		<?php echo $form->textField($model,'Number',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'Number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DocumentTypeID'); ?>
		<?php echo $form->dropDownList($model, 'DocumentTypeID', $typeOptions);?>
		<?php echo $form->error($model,'DocumentTypeID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Title'); ?>
		<?php echo $form->textField($model,'Title',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'Title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'StatusID'); ?>
		<?php echo $form->dropDownList($model, 'StatusID', $statusOptions);?>
		<?php echo $form->error($model,'StatusID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RevisionReference'); ?>
		<?php echo $form->textField($model,'RevisionReference',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'RevisionReference'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Details'); ?>
		<?php echo $form->textField($model,'Details',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'Details'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DateCreated'); ?>	
		<?php echo CHtml::activeDateTimeLocalField($model, 'DateCreated', array()); ?>
		<?php echo $form->error($model,'DateCreated'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ReviewDate'); ?>
		<?php echo CHtml::activeDateTimeLocalField($model, 'ReviewDate', array()); ?>
		<?php echo $form->error($model,'ReviewDate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->