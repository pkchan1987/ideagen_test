<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Congratulations! You have managed to setup this project.</p>

<p>You are required to complete a test and here's the scope:</p>
<ul>
 <li>Implement authentication - We will expect a new person table, auto migration, password in hash etc.</li>
 <li>Create administration module for the Document Status managed list</li>
 <li>Implement date/time picker, dropdown etc in the Document creation and edit page.</li>
</ul>
