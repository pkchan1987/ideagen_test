<?php
/* @var $this DocumentController */
/* @var $model Document */

$this->breadcrumbs=array(
	'Documents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Document Status', 'url'=>array('index')),
	array('label'=>'Manage Document Status', 'url'=>array('admin')),
);
?>

<h1>Create Document</h1>

<?php $this->renderPartial('_form', array('model'=> $model)); ?>