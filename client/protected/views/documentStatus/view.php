<?php
/* @var $this DocumentStatusController */
/* @var $model Document status*/

$this->breadcrumbs=array(
	'Documents Status'=>array('index'),
	$model->Name,
);

$this->menu=array(
	array('label'=>'List Document Status', 'url'=>array('index')),
	array('label'=>'Create Document Status', 'url'=>array('create')),
	array('label'=>'Update Document Status', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Document Status', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Document Status', 'url'=>array('admin')),
);
?>

<h1>View Document Status#<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'Name',
		'DateCreated'
	),
)); ?>
