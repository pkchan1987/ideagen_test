<?php
/* @var $this Document StatusController */
/* @var $model Document Status */

$this->breadcrumbs=array(
	'Document Status'=>array('index'),
	$model->Name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Document Status', 'url'=>array('index')),
	array('label'=>'Create Document Status', 'url'=>array('create')),
	array('label'=>'View Document Status', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Document Status', 'url'=>array('admin')),
);
?>

<h1>Update Document Status <?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_form', array('model'=> $model)); ?>