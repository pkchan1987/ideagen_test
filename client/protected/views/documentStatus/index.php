<?php
/* @var $this DocumentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Document Status',
);

$this->menu=array(
	array('label'=>'Create Document Status', 'url'=>array('create')),
	array('label'=>'Manage Document Status', 'url'=>array('admin')),
);
?>

<h1>Document Status</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
